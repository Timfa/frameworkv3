export default [
    {
        files: ["**/*.js"],
        languageOptions: {
            ecmaVersion: 2023,
            sourceType: "script",
            globals: {
                window: "readonly", // For browser environment globals (e.g., `window`, `document`)
                document: "readonly"
            }
        },
        rules: {
            "no-dupe-keys": "error", // Disallow duplicate keys in object literals
            "curly": ["error", "all"], // Require curly braces for all control statements
            "no-eval": "error", // Disallow the use of eval()
            "no-multi-spaces": "error", // Disallow multiple spaces
            "semi": ["error", "always"], // Require semicolons at the end of statements
            "indent": ["error", "tab"], // Enforce consistent indentation with tabs
            "prefer-const": "error", // Suggest using `const` if a variable is never reassigned
            "no-mixed-spaces-and-tabs": ["error", "smart-tabs"] // Disallow mixed spaces and tabs, allow smart-tabs
        }
    }
];