/**
 * Extending exising functionality
 */

Array.prototype.remove = function (element)
{
	var index = this.indexOf(element);

	if (index > -1)
	{
		this.splice(index, 1);
	}
	else
	{
		console.error("Element not part of array!", this, element);
	}
};

Array.prototype.removeAt = function (index)
{
	if (index > -1)
	{
		this.splice(index, 1);
	}
};

Array.prototype.contains = function (element)
{
	var index = this.indexOf(element);

	return index > -1;
};

Math.clamp = function (number, min, max)
{
	return Math.min(max, Math.max(min, number));
};

Math.randomRange = function (min, max)
{
	return min + (Math.random() * (max - min));
};

Math.lerp = function (a, b, t)
{
	return a + (b - a) * t;
};

Array.prototype.random = function ()
{
	return this[Math.floor((Math.random() * this.length))];
};