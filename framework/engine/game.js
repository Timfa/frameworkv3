class Game
{
	activeScenes = [];
	pendingRemoval = [];

	previousStep = 0; //used for calculating deltaTime

	images = {};

	constructor ()
	{
		this.previousStep = 0;
		this.activeScenes = [];
		this.update = this.update.bind(this);
	}

	startGame ()
	{
		this.update();
		GameInput.g_init();
	}

	preloadImagesThenStart (imageData, onComplete)
	{
		const cont = async () =>
		{
			await this.preloadImages(
				imageData
			);

			this.startGame();
			onComplete && onComplete();
		};

		cont.apply();
	}

	setPrimaryScene (scene)
	{
		if (this.activeScenes.contains(scene))
		{
			for (let i = 0; i < this.activeScenes.length; i++)
			{this.activeScenes[i].g_primary = false;}

			scene.g_primary = true;
		}
	}

	async preloadImages (imagesData)
	{
		const promises = [];
		const context = this;

		for (var imageData of imagesData)
		{
			if (context.images[imageData.name])
			{
				console.warn("Image '" + imageData.name + "' is already loaded! (overwriting anyway)");
			}

			promises.push(new Promise(resolve =>
			{
				var img = new Image();
				var imageContext = { name: imageData.name, subImgTotal: imageData.subImgTotal, perRow: imageData.perRow, loop: imageData.loop, chain: imageData.chain };

				img.onload = function ()
				{

					var finalImage = new image(img, imageContext.subImgTotal, imageContext.perRow);
					finalImage.name = imageContext.name;

					context.images[imageContext.name] = finalImage;

					resolve();
				};

				img.src = imageData.url;
				img.setAttribute("style", "display:none");
			}));
		}
		await Promise.all(promises);

		console.log("all images loaded ", this.images);

		return this.images;
	}

	loadScene (scene)
	{
		if (this.activeScenes.length == 0)
		{scene.g_primary = true;}

		this.activeScenes.push(scene);
		scene.game = this;
		scene.onLoad();
	}

	removeScene (scene)
	{
		const index = this.activeScenes.indexOf(scene);
		if (index >= 0 && !scene.destroyed)
		{
			if (!this.pendingRemoval.contains(index))
			{this.pendingRemoval.push(index);}
		}
	}

	update ()
	{
		this.previousStep = this.previousStep || 0;
		const stepTime = Date.now();
		let stepTimeCurr = (stepTime - this.previousStep) * 0.001;
		stepTimeCurr = Math.min(stepTimeCurr, 0.1);
		if (isNaN(stepTimeCurr))
		{stepTimeCurr = 0;}

		GameInput.g_updateKeys();

		for (let i = 0; i < this.activeScenes.length; i++)
		{
			this.activeScenes[i].update(stepTimeCurr);
			this.activeScenes[i].draw();
		}

		this.cleanActiveScenes();

		this.previousStep = stepTime;

		TWEEN.update();

		requestAnimationFrame(this.update);
	}

	cleanActiveScenes ()
	{
		if (this.pendingRemoval.length > 0)
		{
			this.pendingRemoval.sort((a, b) => a - b);

			for (let i = this.pendingRemoval.length - 1; i >= 0; i--)
			{
				if (this.activeScenes[this.pendingRemoval[i]].destroyed)
				{this.activeScenes.removeAt(this.pendingRemoval[i]);}

				this.pendingRemoval.splice(i, 1);
			}
		}
	}
}