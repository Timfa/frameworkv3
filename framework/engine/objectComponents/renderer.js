class Renderer
{
	sprite = null;
	gameObject = null;
	subImage = 0;
	alpha = 1;
	mirrorX = false;
	mirrorY = false;

	defaultAnimationSpeed = 1;

	cycle = null;

	visible = true;

	constructor (parentObject, sprite)
	{
		this.gameObject = parentObject;
		this.sprite = sprite;
	}

	update (deltaTime) 
	{
		if (this.cycle) 
		{
			if (!this.cycle.cycleSpeed || this.cycle.cycleSpeed == -1) 
			{
				this.subImage = this.cycle.cycleArray[this.cycle.default];
				this.cycle.current = this.cycle.default;
				return;
			}
			this.cycle.timer += deltaTime;

			if (this.cycle.timer > this.cycle.cycleSpeed) 
			{
				this.cycle.timer -= this.cycle.cycleSpeed;
				let next;

				if (this.cycle.loop)
				{
					next = (this.cycle.current + 1) % this.cycle.cycleArray.length;
				}
				else
				{
					if (this.cycle.current + 1 > this.cycle.cycleArray.length)
					{
						if (this.cycle.chain)
						{
							this.cycle.chainDefaultIndex = this.cycle.chainDefaultIndex || this.cycle.default;
							this.cycle.chainSpeed = this.cycle.chainSpeed || this.cycle.cycleSpeed;
							this.setAnimation(this.cycle.chain, this.cycle.chainDefaultIndex, this.cycle.chainSpeed);
						}

						return;
					}
					else
					{
						next = this.cycle.current + 1;
					}
				}

				this.updateCycleSprite(next);
			}
		}
	}

	updateCycleSprite (index)
	{
		this.subImage = this.cycle.cycleArray[index];
		this.cycle.current = index;
	}

	draw ()
	{
		if (this.sprite)
		{
			var offX = this.gameObject.scene.cameraPosition.x;
			var offY = this.gameObject.scene.cameraPosition.y;

			this.drawSprite(this.gameObject.transform.position.x + offX,
				this.gameObject.transform.position.y + offY,
				this.gameObject.transform.size.x,
				this.gameObject.transform.size.y,
				this.gameObject.transform.rotation);
		}
	}

	drawSprite ()
	{
		if (isNaN(this.subImage))
		{
			this.subImage = this.g_subImgOld;
		}
		else
		{
			this.g_subImgOld = this.subImage;
		}

		this.sprite.draw(
			this.gameObject.scene,
			Math.round(this.gameObject.transform.position.x),
			Math.round(this.gameObject.transform.position.y),
			this.gameObject.transform.size.x,
			this.gameObject.transform.size.y,
			this.gameObject.transform.rotation,
			this.mirrorX,
			this.mirrorY,
			this.alpha,
			this.subImage
		);
	}

	get currentAnimationName ()
	{
		return this.cycle ? this.cycle.label : null;
	}

	setAnimation (label, defaultIndex = 0, cycleSpeed = -1) 
	{
		if (cycleSpeed < 0)
		{
			cycleSpeed = this.defaultAnimationSpeed;
		}

		if (this.cycle) 
		{
			if (this.cycle.label == label) 
			{
				this.cycle.defaultIndex = defaultIndex;
				this.cycle.cycleSpeed = cycleSpeed;
				return;
			}
		}

		const cycleData = this.sprite.animationCycles[label];

		this.cycle = {
			label: label,
			cycleArray: cycleData.frames,
			default: defaultIndex,
			current: defaultIndex,
			cycleSpeed: cycleSpeed,
			loop: cycleData.loop,
			chain: cycleData.chain,
			chainDefaultIndex: cycleData.chainDefaultIndex,
			chainSpeed: cycleData.chainSpeed,
			timer: 0
		};

		this.subImage = this.cycle.cycleArray[this.cycle.current];
	}

	setAnimationCycleSpeed (cycleSpeed) 
	{
		if (this.cycle) 
		{
			this.cycle.cycleSpeed = cycleSpeed;
		}
	}

	setSubImage (subImage) 
	{
		this.subImage = subImage;
		this.cycle = null;
	}
}