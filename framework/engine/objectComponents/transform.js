class Transform
{
	get position ()
	{
		if (!this.g_position)
		{this.g_position = new vector(0, 0);}

		return this.g_position;
	}

	set position (v)
	{
		if (!this.g_position)
		{this.g_position = new vector(v.x, v.y);}

		this.g_position.x = v.x;
		this.g_position.y = v.y;
	}

	get scale ()
	{
		if (!this.g_scale)
		{this.g_scale = new vector(0, 0);}

		return this.g_scale;
	}

	set scale (v)
	{
		if (!this.g_scale)
		{this.g_scale = new vector(v.x, v.y);}

		this.g_scale.x = v.x;
		this.g_scale.y = v.y;
	}

	get velocity ()
	{
		if (!this.g_velocity)
		{this.g_velocity = new vector(0, 0);}

		return this.g_velocity;
	}

	set velocity (v)
	{
		if (!this.g_velocity)
		{this.g_velocity = new vector(v.x, v.y);}

		this.g_velocity.x = v.x;
		this.g_velocity.y = v.y;
	}

	g_position = null;

	g_scale = null;
	rotation = 0;
	gameObject = null;

	g_velocity = null;
	angularVelocity = 0;

	get size ()
	{
		return new vector(
			this.scale.x * this.gameObject.renderer.sprite.width,
			this.scale.y * this.gameObject.renderer.sprite.height);
	}

	constructor (gameObject, x, y, w, h, r)
	{
		this.gameObject = gameObject;
		this.position = new vector(x, y);
		this.scale = new vector(w, h);
		this.rotation = r;

		this.velocity = new vector(0, 0);
	}

	update (deltaTime)
	{
		this.position = this.position.add(this.velocity.stretch(deltaTime));
		this.rotation += this.angularVelocity * deltaTime;
	}

	pointTowards (point)
	{
		var delta = point.subtract(this.position);

		this.rotation = delta.toAngle();
	}

	get left ()
	{
		return this.g_dir(0);
	}

	get right ()
	{
		return this.g_dir(180);
	}

	get forward ()
	{
		return this.g_dir(90);
	}

	get back ()
	{
		return this.g_dir(270);
	}

	g_dir (offset)
	{
		var myAngleInRadians = (this.rotation + offset) * (Math.PI / 180);

		return new vector(-Math.cos(myAngleInRadians), -Math.sin(myAngleInRadians));
	}
}