class Collider
{
	gameObject = null;
	enabled = false;

	constructor (gameObject)
	{
		this.gameObject = gameObject;
	}

	get widestDiagonal ()
	{
		return Math.sqrt((this.gameObject.transform.size.x * this.gameObject.transform.size.x) + (this.gameObject.transform.size.y * this.gameObject.transform.size.y));
	}

	update (deltaTime)
	{
		if (this.enabled)
		{
			this.checkCollisions();
		}

		const pressed = GameInput.mousePressed();
		const held = GameInput.mouseHeld();

		if (pressed.left || pressed.middle || pressed.right)
		{
			this.checkClick(true, pressed);
		}

		if (held.left || held.middle || held.right)
		{
			this.checkClick(false, held);
		}
	}

	checkClick (thisFrame, btns)
	{
		var hit = false;
		var clickPos = GameInput.mousePosition.subtract(this.gameObject.scene.position).subtract(this.gameObject.scene.cameraPosition);

		var hw = this.gameObject.transform.size.x / 2;
		var hh = this.gameObject.transform.size.y / 2;

		if (clickPos.x > this.gameObject.transform.position.x - hw && clickPos.x < this.gameObject.transform.position.x + hw)
		{
			if (clickPos.y > this.gameObject.transform.position.y - hh && clickPos.y < this.gameObject.transform.position.y + hh)
			{
				hit = true;
			}
		}

		if (hit)
		{
			if (thisFrame)
			{
				this.gameObject.onClick && this.gameObject.onClick(btns);
			}
			else
			{
				this.gameObject.onMouseHeld && this.gameObject.onMouseHeld(btns);
			}
		}
	}

	isInBounds (point)
	{
		var hw = this.gameObject.transform.size.x / 2;
		var hh = this.gameObject.transform.size.y / 2;

		if (point.x > this.gameObject.transform.position.x - hw && point.x < this.gameObject.transform.position.x + hw)
		{
			if (point.y > this.gameObject.transform.position.y - hh && point.y < this.gameObject.transform.position.y + hh)
			{
				return true;
			}
		}

		return false;
	}

	checkCollisions ()
	{
		if (this.enabled && !this.gameObject.destroyed)
		{
			const offset = this.gameObject.scene.activeObjects.indexOf(this) + 1;

			for (var i = offset; i < this.gameObject.scene.activeObjects.length; i++)
			{
				if (this.gameObject.scene.activeObjects[i] != this && this.gameObject.scene.activeObjects[i].collider.enabled)
				{
					if (this.widestDiagonal + this.gameObject.scene.activeObjects[i].collider.widestDiagonal < this.gameObject.transform.position.distanceTo(this.gameObject.scene.activeObjects[i].transform.position))
					{ continue; }

					var hit = false;
					var o2 = this.gameObject.scene.activeObjects[i];

					//hw = halfWidth, hh = halfHeight
					var hw = this.gameObject.transform.size.x / 2;
					var hh = this.gameObject.transform.size.y / 2;

					var ohw = o2.transform.size.x / 2;
					var ohh = o2.transform.size.y / 2;

					//CHECK FROM OBJ PERSPECTIVE
					var points =
						[
							this.gameObject.transform.position.x - hw,
							this.gameObject.transform.position.x + hw,
							this.gameObject.transform.position.y - hh,
							this.gameObject.transform.position.y + hh
						];

					if (points[1] >= o2.transform.position.x - ohw && points[0] <= o2.transform.position.x + ohw)
					{
						if (points[3] >= o2.transform.position.y - ohh && points[2] <= o2.transform.position.y + ohh)
						{
							hit = true;
						}
					}

					if (hit)
					{ if (!this.gameObject.destroyed && typeof this.gameObject.onCollision == 'function') { this.gameObject.onCollision(o2); } }
				}
			}
		}
	}
}