class Scene
{
	static idCount = 0;
	id = null;
	activeObjects = [];
	position = null;
	size = null;
	real_position = null;
	px = 0; py = 0;
	real_size = null;
	cameraPosition = null;
	parentScene = null;
	renderBackground = true;
	quadTree = null;

	pendingRemoval = [];

	g_primary = false;

	static DisplayModes = { absolute: 0, relativeToParent: 1 };

	displayMode = Scene.DisplayModes.absolute;

	cv_canvas = null;
	cv_context = null;
	destroyed = false;

	game = null;

	get primary ()
	{
		return this.g_primary;
	}

	constructor (x, y, width, height, displayMode = 0)
	{
		Scene.idCount++;
		this.id = 'canvas_' + Scene.idCount;

		this.position = new vector(x, y);
		this.size = new vector(width, height);
		this.real_position = new vector(x, y);
		this.real_size = new vector(width, height);
		this.cameraPosition = new vector(0, 0);
		this.displayMode = displayMode;


		const body = document.body;

		body.insertAdjacentHTML('beforeend', '<canvas id="' + this.id + '" style="position: absolute; left: ' + x + 'px; top: ' + y + 'px;" width="' + width + '" height="' + height + '"></canvas>');

		this.cv_canvas = document.getElementById(this.id);
		this.cv_context = this.cv_canvas.getContext('2d');

		this.g_fixSize();
		this.quadTree = new QuadTree(this.real_size.x, this.real_size.y);
	}

	setPrimary ()
	{
		this.game.setPrimaryScene(this);
	}

	onLoad ()
	{

	}

	loadChildScene (scene)
	{
		scene.parentScene = this;
		this.game.loadScene(scene);
	}

	destroy ()
	{
		if (!this.destroyed)
		{
			for (var i = 0; i < this.activeObjects.length; i++)
			{
				this.activeObjects[i].destroy();
			}
			this.game.removeScene(this);

			//this.context.clearRect(0, 0, this.real_size.x, this.real_size.y);

			this.removeCanvas();

			if (this.primary && this.parentScene)
			{
				this.parentScene.setPrimary();
			}

			this.destroyed = true;
		}
	}

	removeCanvas ()
	{
		var elem = document.getElementById(this.id);
		return elem.parentNode.removeChild(elem);
	}

	backgroundColor =
		{
			r: 255,
			g: 255,
			b: 255
		};

	get canvas ()
	{
		return this.cv_canvas;
	}

	get context ()
	{
		return this.cv_context;
	}

	addObject (obj)
	{
		if (this.destroyed)
		{return;}

		this.activeObjects.push(obj);
		obj.scene = this;
		obj.initQuadtree(this.quadTree);
		obj.checkQuadtree();

		obj.addedToScene && obj.addedToScene();

		return obj;
	}

	removeObject (obj)
	{
		const index = this.activeObjects.indexOf(obj);
		if (index >= 0 && !obj.destroyed)
		{
			if (!this.pendingRemoval.contains(index))
			{this.pendingRemoval.push(index);}
		}

		if (this.quadTree === obj.quadTree)
		{this.quadTree.remove(obj);}
	}

	update (deltaTime)
	{
		for (let i = 0; i < this.activeObjects.length; i++)
		{
			this.activeObjects[i].update(deltaTime);
		}

		if (this.pendingRemoval.length)
		{
			this.pendingRemoval.sort((a, b) => a - b);

			for (let i = this.pendingRemoval.length - 1; i >= 0; i--) 
			{
				if (this.activeObjects[this.pendingRemoval[i]].destroyed)
				{this.activeObjects.removeAt(this.pendingRemoval[i]);}

				this.pendingRemoval.splice(i, 1);
			}
		}

		this.quadTree.update(deltaTime);
	}

	g_fixSize ()
	{
		if (this.displayMode == Scene.DisplayModes.relativeToParent)
		{
			if (this.parentScene)
			{
				this.real_position.x = this.parentScene.real_position.x + (this.position.x * this.parentScene.real_size.x);
				this.real_position.y = this.parentScene.real_position.y + (this.position.y * this.parentScene.real_size.y);

				this.real_size.x = this.parentScene.real_size.x * this.size.x;
				this.real_size.y = this.parentScene.real_size.y * this.size.y;
			}
			else
			{
				this.real_position.x = window.innerWidth * this.position.x;
				this.real_position.y = window.innerHeight * this.position.y;

				this.real_size.x = window.innerWidth * this.size.x;
				this.real_size.y = window.innerHeight * this.size.y;
			}
		}
		else if (this.displayMode == Scene.DisplayModes.absolute)
		{
			this.real_position.x = this.position.x;
			this.real_position.y = this.position.y;

			this.real_size.x = this.size.x;
			this.real_size.y = this.size.y;
		}

		if (this.canvas.width != this.real_size.x)
		{this.canvas.width = this.real_size.x;}

		if (this.canvas.height != this.real_size.y)
		{this.canvas.height = this.real_size.y;}

		if (this.px != this.real_position.x)
		{
			this.px = this.real_position.x;
			this.canvas.style.left = this.real_position.x;
		}

		if (this.py != this.real_position.y)
		{
			this.py = this.real_position.y;
			this.canvas.style.top = this.real_position.y;
		}
	}

	draw ()
	{
		if (this.destroyed)
		{return;}

		this.g_fixSize();

		if (this.renderBackground)
		{
			this.context.fillStyle = "rgb(" + this.backgroundColor.r + "," + this.backgroundColor.g + "," + this.backgroundColor.b + ")"; // sets the color to fill in the rectangle with
			this.context.fillRect(0, 0, this.real_size.x, this.real_size.y);
		}
		else 
		{
			this.context.clearRect(0, 0, this.real_size.x, this.real_size.y);
		}

		const sortedActiveObjects = this.activeObjects.slice().sort((a, b) => a.layer - b.layer);

		for (var i = 0; i < sortedActiveObjects.length; i++)
		{
			if (!sortedActiveObjects[i].destroyed && sortedActiveObjects[i].renderer.visible)
			{sortedActiveObjects[i].draw(this.context);}
		}
	}
}