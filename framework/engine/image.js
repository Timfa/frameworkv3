class image
{
	subImages = null;
	subImagesPerRow = null;
	loop = false;
	chain = null;

	g_image = null;

	animationCycles = {};

	constructor (image, subImgTotal = 1, perRow = 1, loop = false, chain = null)
	{
		subImgTotal = subImgTotal || 1;
		perRow = perRow || 1;

		this.subImages = subImgTotal;
		this.subImagesPerRow = perRow;
		this.g_image = image;
		this.loop = loop;
		this.chain = chain;

		this.cacheWidth = -1;
		this.cacheHeight = -1;

		image.id = "loadedImage";
		document.body.append(image);
		console.log("Image loaded: " + image.src);
	}

	draw (scene, dx, dy, dw, dh, rotation, mirrorX, mirrorY, alpha, subImage)
	{
		var shouldDraw = true;

		if (dx < -Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
		{shouldDraw = false;}

		if (dy < -Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
		{shouldDraw = false;}

		if (dx > scene.real_size.x + Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
		{shouldDraw = false;}

		if (dy > scene.real_size.y + Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
		{shouldDraw = false;}

		if (shouldDraw)
		{
			var context = scene.context;
			context.save();
			var oldAlpha = context.globalAlpha;
			context.globalAlpha = alpha;

			context.translate(dx, dy);
			context.rotate(rotation * (Math.PI / 180));

			(mirrorX || mirrorY) && context.scale(mirrorX ? -1 : 1, mirrorY ? -1 : 1);

			var subImageData = this.getSubImage(subImage);

			context.drawImage(this.g_image, subImageData.sx, subImageData.sy, subImageData.sw, subImageData.sh, -dw / 2, -dh / 2, dw, dh);

			context.restore();
			context.globalAlpha = oldAlpha;
		}
	}

	hasAnimationCycle (label)
	{
		return this.animationCycles[label] != null;
	}

	addAnimationCycle (label, cycle, loop, chain, chainDefaultIndex, chainSpeed) 
	{
		this.animationCycles[label] =
		{
			frames: cycle,
			loop: loop,
			chain: chain,
			chainDefaultIndex: chainDefaultIndex,
			chainSpeed: chainSpeed
		};
	}

	get width () 
	{
		if (this.cacheWidth < 0)
		{this.cacheWidth = this.g_image.width / this.subImagesPerRow;}

		return this.cacheWidth;
	}

	get height () 
	{
		if (this.cacheHeight < 0)
		{this.cacheHeight = this.g_image.height / (Math.ceil(this.subImages / this.subImagesPerRow) + 0);}

		return this.cacheHeight;
	}

	cacheWidth = -1;
	cacheHeight = -1;

	getSubImage (subImageIndex) 
	{
		var subImage = Math.round(subImageIndex);
		subImage %= this.subImages;

		var c = subImage % this.subImagesPerRow;
		var r = Math.floor(subImage / this.subImagesPerRow);
		var sw = (this.g_image.width / this.subImagesPerRow);
		var sh = (this.g_image.height / Math.ceil(this.subImages / this.subImagesPerRow));
		var sx = sw * c;
		var sy = sh * r;

		return { sw: sw, sh: sh, sx: sx, sy: sy };
	}
}