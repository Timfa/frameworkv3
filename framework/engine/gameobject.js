class GameObject
{
	transform = null;
	renderer = null;
	collider = null;
	scene = null;
	layer = 0;
	tags = [];

	destroyed = false;

	constructor (x, y, sprite, layer = 0)
	{
		this.transform = new Transform(this, x, y, 1, 1, 0);
		this.renderer = new Renderer(this, sprite);
		this.collider = new Collider(this);
		this.layer = layer;
	}

	initQuadtree (quadTree)
	{
		this.quadTree = quadTree;
	}

	checkQuadtree ()
	{
		if (!this.quadTree)
		{return;}

		if (this.node)
		{
			if (this.transform.position.x < this.node.x1
                || this.transform.position.x > this.node.x2
                || this.transform.position.y < this.node.y1
                || this.transform.position.y > this.node.y2)
			{
				this.quadTree.remove(this);
				this.quadTree.add(this);
			}
		}
		else
		{
			this.quadTree.add(this);
		}
	}

	update (deltaTime)
	{
		this.transform.update(deltaTime);
		this.collider.update(deltaTime);
		this.renderer.update(deltaTime);

		if (!this.destroyed)
		{this.checkQuadtree();}
	}

	destroy ()
	{
		if (!this.destroyed)
		{
			this.onDestroy && this.onDestroy();
			this.quadTree && this.quadTree.remove(this);
			this.scene.removeObject(this);
			this.destroyed = true;
		}
	}

	draw ()
	{
		this.renderer.draw();
	}
}