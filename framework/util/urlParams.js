var UrlParams =
{
	get: function (key)
	{
		var url_string = window.location.href;
		var url = new URL(url_string);
		var c = url.searchParams.get(key);

		return c;
	},

	set: function (key, value)
	{
		var url_string = window.location.href;
		var url = new URL(url_string);
		url.searchParams.set(key, value);

		window.history.pushState(null, {}, url);
	}
};