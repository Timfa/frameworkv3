var CookieManager =
{
	setCookie: function (cname, cvalue, exdays = 365) 
	{
		const d = new Date();

		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		const expires = "expires=" + d.toUTCString();

		const newCookie = cname + "=" + cvalue + ";" + expires + ";path=kiop/";
		document.cookie = newCookie;
	},

	getCookie: function (cname) 
	{
		const name = cname + "=";
		const ca = document.cookie.split(';');

		for (let i = 0; i < ca.length; i++) 
		{
			let c = ca[i];

			while (c.charAt(0) == ' ') 
			{
				c = c.substring(1);
			}

			if (c.indexOf(name) == 0) 
			{
				return c.substring(name.length, c.length);
			}
		}

		return "";
	},

	deleteCookie: function (cname) 
	{
		const d = new Date();

		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		const expires = "expires=Thu, 01 Jan 1970 00:00:00 UTC";

		document.cookie = cname + "=delete;" + expires + ";path=/";
	}
};