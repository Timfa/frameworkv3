

var GameInput =
{
	keys: {},
	g_keyStatus: [],
	g_pKeyStatus: [],

	g_pressKeyStatus: [],
	g_ppressKeyStatus: [],

	g_pressBlacklist: [],

	g_mouseDown: false,
	g_mouseDownThisFrame: false,

	g_altMouseDown: false,
	g_altMouseDownThisFrame: false,

	g_middleMouseDown: false,
	g_middleMouseDownThisFrame: false,

	trueMousePosition: null,
	mousePosition: null,

	MouseButtons: {
		Left: 0,
		Middle: 1,
		Right: 2
	},

	g_init: function ()
	{
		trueMousePosition = new vector(0, 0);
		mousePosition = new vector(0, 0);

		document.onkeydown = GameInput.g_setKeyStatus;
		document.onkeyup = GameInput.g_setKeyStatus;

		document.oncontextmenu = function () { return false; };

		document.onmousedown = function (e)
		{
			switch (e.button)
			{
			case GameInput.MouseButtons.Left: GameInput.g_mouseDownThisFrame = true; break;
			case GameInput.MouseButtons.Middle: GameInput.g_middleMouseDownThisFrame = true; break;
			case GameInput.MouseButtons.Right: GameInput.g_altMouseDownThisFrame = true; break;
			}
		};

		document.onmouseup = function (e)
		{
			switch (e.button)
			{
			case GameInput.MouseButtons.Left:
				GameInput.g_mouseDownThisFrame = false;
				GameInput.g_mouseDown = false;
				break;
			case GameInput.MouseButtons.Middle:
				GameInput.g_middleMouseDownThisFrame = false;
				GameInput.g_middleMouseDown = false;
				break;
			case GameInput.MouseButtons.Right:
				GameInput.g_altMouseDownThisFrame = false;
				GameInput.g_altMouseDown = false;
				break;
			}
		};

		document.onmousemove = function (e)
		{
			if (!GameInput.trueMousePosition)
			{GameInput.trueMousePosition = new vector();}

			GameInput.trueMousePosition.x = e.x;
			GameInput.trueMousePosition.y = e.y;
		};

		var el = document.getElementsByTagName("body")[0];
		el.addEventListener("touchstart", function () { GameInput.g_mouseDownThisFrame = true; }, false);
		el.addEventListener("touchend", function () { GameInput.g_mouseDownThisFrame = false; GameInput.g_mouseDown = false; }, false);
		el.addEventListener("touchcancel", function () { GameInput.g_mouseDownThisFrame = false; GameInput.g_mouseDown = false; }, false);
		el.addEventListener("touchmove", function () { return false; }, false);
	},

	g_setKeyStatus: function (e)
	{
		GameInput.keys[e.key] = e.which;
		GameInput.g_keyStatus[e.which] = (e.type == "keydown");

		if (!GameInput.g_pressBlacklist[e.which])
		{GameInput.g_pressKeyStatus[e.which] = (e.type == "keydown");}

		GameInput.g_pressBlacklist[e.which] = (e.type == "keydown"); //only allow single presses if key has been released once
	},

	g_updateKeys: function ()
	{
		GameInput.mousePosition = GameInput.trueMousePosition;

		GameInput.g_pKeyStatus = GameInput.g_keyStatus;
		GameInput.g_ppressKeyStatus = GameInput.g_pressKeyStatus;
		GameInput.g_pressKeyStatus = [];

		if (GameInput.g_mouseDown && GameInput.g_mouseDownThisFrame)
		{
			GameInput.g_mouseDownThisFrame = false;
		}

		if (GameInput.g_mouseDownThisFrame && !GameInput.g_mouseDown)
		{
			GameInput.g_mouseDown = true;
		}

		if (GameInput.g_altMouseDown && GameInput.g_altMouseDownThisFrame)
		{
			GameInput.g_altMouseDownThisFrame = false;
		}

		if (GameInput.g_altMouseDownThisFrame && !GameInput.g_altMouseDown)
		{
			GameInput.g_altMouseDown = true;
		}

		if (GameInput.g_middleMouseDown && GameInput.g_middleMouseDownThisFrame)
		{
			GameInput.g_middleMouseDownThisFrame = false;
		}

		if (GameInput.g_middleMouseDownThisFrame && !GameInput.g_middleMouseDown)
		{
			GameInput.g_middleMouseDown = true;
		}
	},


	isPressed: function (keyCode)
	{
		if (GameInput.g_ppressKeyStatus[keyCode] == null)
		{return false;}
		else
		{return GameInput.g_ppressKeyStatus[keyCode];}
	},


	isHeld: function (keyCode)
	{
		if (GameInput.g_pKeyStatus[keyCode] == null)
		{return false;}
		else
		{return GameInput.g_pKeyStatus[keyCode];}
	},

	mousePressed: function (btn)
	{
		switch (btn)
		{
		case GameInput.MouseButtons.Left: return GameInput.g_mouseDownThisFrame;
		case GameInput.MouseButtons.Middle: return GameInput.g_middleMouseDownThisFrame;
		case GameInput.MouseButtons.Right: return GameInput.g_altMouseDownThisFrame;
		}

		return { left: GameInput.g_mouseDownThisFrame, middle: GameInput.g_middleMouseDownThisFrame, right: GameInput.g_altMouseDownThisFrame };
	},

	mouseHeld: function (btn)
	{
		switch (btn)
		{
		case GameInput.MouseButtons.Left: return GameInput.g_mouseDown;
		case GameInput.MouseButtons.Middle: return GameInput.g_middleMouseDown;
		case GameInput.MouseButtons.Right: return GameInput.g_altMouseDown;
		}

		return { left: GameInput.g_mouseDown, middle: GameInput.g_middleMouseDown, right: GameInput.g_altMouseDown };
	}
};//