class vector
{
	x = 0;
	y = 0;

	constructor (x, y)
	{
		this.x = x;

		this.y = y;
	}

	get length ()
	{
		return Math.sqrt((this.x * this.x) + (this.y * this.y));
	}

	getLength ()
	{
		return Math.sqrt((this.x * this.x) + (this.y * this.y));
	}


	toAngle ()
	{
		let angle = (Math.atan2(this.y, this.x) * 180 / Math.PI) + 90;
		if (angle < 0)
		{angle += 360;}
		return angle;
	}

	static fromAngle (angle)
	{
		angle = 360 - ((angle + 180) % 360);
		angle = (angle / 180) * Math.PI;
		return new vector(Math.sin(angle), Math.cos(angle));
	}

	normalize ()
	{
		var newVector = this.getCopy();

		if (newVector.length != 0)
		{
			var length = newVector.length;
			newVector.x /= length;
			newVector.y /= length;
		}

		return newVector;
	}


	lerpTo (otherVector, t)
	{
		var delta = otherVector.subtract(this);

		return this.add(delta.stretch(t));
	}


	distanceTo (otherVector)
	{
		var dX = this.x - otherVector.x;
		var dY = this.y - otherVector.y;

		return new vector(dX, dY).length;
	}


	add (otherVector)
	{
		var newVector = this.getCopy();

		newVector.x += otherVector.x;
		newVector.y += otherVector.y;

		return newVector;
	}


	subtract (otherVector)
	{
		var newVector = this.getCopy();

		newVector.x -= otherVector.x;
		newVector.y -= otherVector.y;

		return newVector;
	}


	multiply (otherVector)
	{
		var newVector = this.getCopy();

		newVector.x *= otherVector.x;
		newVector.y *= otherVector.y;

		return newVector;
	}


	divide (otherVector)
	{
		var newVector = this.getCopy();

		newVector.x /= otherVector.x;
		newVector.y /= otherVector.y;

		return newVector;
	}


	stretch (length)
	{
		var newVector = this.getCopy();

		newVector.x *= length;
		newVector.y *= length;

		return newVector;
	}

	rotate (degrees)
	{
		var ca = Math.cos(degrees * (Math.PI / 180));
		var sa = Math.sin(degrees * (Math.PI / 180));
		return new vector(ca * this.x - sa * this.y, sa * this.x + ca * this.y);
	}

	deltaAngle (otherVector)
	{
		var a = this.toAngle() * (Math.PI / 180);
		var b = otherVector.toAngle() * (Math.PI / 180);

		return Math.atan2(Math.sin(a - b), Math.cos(a - b)) * (180 / Math.PI);
	}

	cross (otherVector)
	{
		return this.x * otherVector.y - this.y * otherVector.x;
	}


	getCopy ()
	{
		return new vector(this.x, this.y);
	}
};