var GameAudio =
{
	active: false,

	init: function (collection, containingFolder)
	{
		console.log("GAME: Loading sounds. (Note: Only supported from online-source, will fail when executed on local files.)");

		try 
		{
			var soundColl = [];

			for (var i = 0; i < collection.length; i++)
			{
				collection[i].preload = true;

				soundColl.push(collection[i]);
			}

			if (containingFolder == null)
			{containingFolder = "";}

			if (containingFolder != "")
			{containingFolder += "/";}

			ion.sound
			({
				sounds: soundColl,
				volume: 1,
				preload: true,
				multiplay: true,
				path: containingFolder
			});

			this.active = true;
		}
		catch (err) 
		{
			console.log("GAME: ERROR: Sounds could not be loaded. Sound will not be available.");
		}
	},

	play: function (name)
	{
		if (this.active)
		{ion.sound.play(name);}
	}
};