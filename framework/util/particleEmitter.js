class ParticleEmitter
{
	static pool = [];
	enabled = false;
	mine = [];
	position = null;

	emissionRate = 0;
	emissionTimer = 0;
	emissionAngle = 0;

	startRotation = 0;
	startVelocity = 0;
	startAngularVelocity = 0;
	acceleration = null;
	angularAcceleration = 0;
	sprite = null;
	startSize = null;
	endSize = null;
	lifetimeMin = 0;
	lifetimeMax = 0;
	fadeIn = 0;
	fadeOut = 0;
	animationSpeed = 0;
	startFrame = 0;

	friction = null;
	angularFriction = 0;

	constructor (position, sprite, emissionRate, emissionAngle,
		startRotation, startVelocity, startAngularVelocity, acceleration,
		angularAcceleration, startSize, endSize, lifetimeMin,
		lifetimeMax = -1, fadeIn = 0, fadeOut = 0, startFrame = 0, animationSpeed = 0)
	{
		if (lifetimeMax < 0)
		{lifetimeMax = lifetimeMin;}

		this.position = position;
		this.sprite = sprite;

		this.emissionRate = emissionRate;
		this.emissionTimer = emissionRate;
		this.emissionAngle = emissionAngle;

		this.startRotation = startRotation;
		this.startVelocity = startVelocity * 10;
		this.startAngularVelocity = startAngularVelocity * 10;
		this.acceleration = acceleration.stretch(10);
		this.angularAcceleration = angularAcceleration * 10;
		this.startSize = startSize;
		this.endSize = endSize;
		this.lifetimeMin = lifetimeMin;
		this.lifetimeMax = lifetimeMax;
		this.fadeIn = fadeIn;
		this.fadeOut = fadeOut;
		this.startFrame = startFrame;
		this.animationSpeed = animationSpeed;

		this.friction = new vector(0, 0);
	}

	start ()
	{
		this.enabled = true;
	}

	stop ()
	{
		this.enabled = false;
	}

	emit (amount = 1)
	{
		for (let i = 0; i < amount; i++)
		{
			const newParticle = ParticleEmitter.unPool();
			this.mine.push(newParticle);

			const velocity = vector.fromAngle(this.emissionAngle).stretch(this.startVelocity);
			newParticle.emit(this, this.position.x, this.position.y, this.startRotation,
				velocity.x, velocity.y, this.startAngularVelocity, this.acceleration.x,
				this.acceleration.y, this.angularAcceleration, this.sprite, this.startSize.x,
				this.startSize.y, this.endSize.x, this.endSize.y, this.lifetimeMin,
				this.lifetimeMax, this.fadeIn, this.fadeOut, this.startFrame, this.animationSpeed,
				this.friction, this.angularFriction);
		}
	}

	update (deltaTime)
	{
		if (this.enabled)
		{this.emissionTimer -= deltaTime;}

		if (this.emissionTimer <= 0)
		{
			this.emissionTimer = this.emissionRate;
			this.emit();
		}

		for (let i = 0; i < this.mine.length; i++)
		{
			this.mine[i].update(deltaTime);
		}
	}

	draw (scene)
	{
		for (let i = 0; i < this.mine.length; i++)
		{
			this.mine[i].draw(scene);
		}
	}

	dispose (particle)
	{
		this.mine.remove(particle);
		ParticleEmitter.returnToPool(particle);
	}

	static unPool () //get a particle from the static pool
	{
		if (ParticleEmitter.pool.length > 0)
		{
			return ParticleEmitter.pool.pop();
		}

		return new Particle();
	}

	static returnToPool (particle)
	{
		if (!ParticleEmitter.pool.contains(particle))
		{
			ParticleEmitter.pool.push(particle);
		}
	}
}

class Particle
{
	parent = null;
	position = null;
	rotation = 0;
	velocity = null;
	angularVelocity = 0;
	acceleration = null;
	angularAcceleration = 0;
	sprite = null;
	animTimer = 0;
	animationSpeed = 0;
	subImage = 0;
	startSize = null;
	endSize = null;
	fadeInTime = 0;
	fadeInStart = 0;
	fadeOutTime = 0;
	lifeTime = 0;
	lifeTimeStart = 0;
	friction = null;
	angularfriction = 0;

	constructor ()
	{
		this.position = new vector(0, 0);
		this.velocity = new vector(0, 0);
		this.acceleration = new vector(0, 0);
		this.startSize = new vector(0, 0);
		this.endSize = new vector(0, 0);
	}

	emit (parent, x, y, r, vx, vy, va, ax, ay, ar, spr, ssx, ssy, esx, esy, lmin, lmax, fi = 0, fo = 0, sf = 0, as = 0, fric, anfric)
	{
		this.parent = parent;
		this.position.x = x;
		this.position.y = y;
		this.rotation = r;
		this.velocity.x = vx;
		this.velocity.y = vy;
		this.angularVelocity = va;
		this.acceleration.x = ax;
		this.acceleration.y = ay;
		this.angularAcceleration = ar;
		this.sprite = spr;
		this.startSize.x = ssx;
		this.startSize.y = ssy;
		this.endSize.x = esx;
		this.endSize.y = esy;
		this.lifeTime = Math.randomRange(lmin, lmax);
		this.lifeTimeStart = this.lifeTime;
		this.fadeInTime = fi;
		this.fadeInStart = fi;
		this.fadeOutTime = fo;
		this.animationSpeed = as;
		this.animTimer = as;
		this.subImage = sf;
		this.friction = fric;
		this.angularFriction = anfric;
	}

	update (deltaTime)
	{
		this.position.x += this.velocity.x * deltaTime;
		this.position.y += this.velocity.y * deltaTime;
		this.velocity.x += this.acceleration.x * deltaTime;
		this.velocity.y += this.acceleration.y * deltaTime;

		this.velocity.x += this.friction.x * deltaTime * -Math.sign(this.velocity.x * 10);
		this.velocity.y += this.friction.y * deltaTime * -Math.sign(this.velocity.y * 10);

		this.rotation += this.angularVelocity * deltaTime;
		this.angularVelocity += this.angularAcceleration * deltaTime;

		this.angularVelocity += this.angularFriction * deltaTime * -Math.sign(this.angularVelocity * 10);

		this.lifeTime -= deltaTime;

		if (this.fadeInTime > 0)
		{this.fadeInTime -= deltaTime;}

		if (this.animationSpeed > 0)
		{
			this.animTimer -= deltaTime;

			if (this.animTimer <= 0)
			{
				this.animTimer = this.animationSpeed;
				this.subImage = this.subImage + 1 % this.sprite.subImages;
			}
		}

		if (this.lifeTime <= 0)
		{this.parent.dispose(this);}
	}

	draw (scene)
	{
		const foAlpha = Math.min(1, this.lifeTime / this.fadeOutTime);

		const fiAlpha = 1 - (this.fadeInTime / this.fadeInStart);

		this.sprite.draw(
			scene,
			Math.round(this.position.x),
			Math.round(this.position.y),
			Math.lerp(this.endSize.x, this.startSize.x, this.lifeTime / this.lifeTimeStart),
			Math.lerp(this.endSize.y, this.startSize.y, this.lifeTime / this.lifeTimeStart),
			this.rotation,
			false,
			false,
			Math.min(fiAlpha, foAlpha),
			this.subImage
		);
	}
}