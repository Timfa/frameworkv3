class Ray
{
	static Cast (scene, origin, direction, maxLength = -1)
	{
		direction = direction.normalize();
		const hits = []; // {hitObj, hitPoint}
		for (var i = 0; i < scene.activeObjects.length; i++)
		{
			let castAngle = direction.toAngle();

			if (scene.activeObjects[i] != this && scene.activeObjects[i].collider.enabled)
			{
				var hw = scene.activeObjects[i].transform.size.x / 2;
				var hh = scene.activeObjects[i].transform.size.y / 2;

				const points =
					[
						new vector(scene.activeObjects[i].transform.position.x - hw, scene.activeObjects[i].transform.position.y - hh),
						new vector(scene.activeObjects[i].transform.position.x + hw, scene.activeObjects[i].transform.position.y - hh),
						new vector(scene.activeObjects[i].transform.position.x - hw, scene.activeObjects[i].transform.position.y + hh),
						new vector(scene.activeObjects[i].transform.position.x + hw, scene.activeObjects[i].transform.position.y + hh)
					];

				points.sort((p1, p2) => p1.subtract(origin).length - p2.subtract(origin).length).pop();

				for (let p = 0; p < points.length; p++)
				{
					const point = points[p];
					const deltaDistance = point.subtract(origin).length;

					if (deltaDistance > maxLength && maxLength > 0)
					{
						continue;
					}

					const stretched = direction.stretch(Math.min(deltaDistance, maxLength > 0 ? maxLength : deltaDistance));

					const calcPoint = origin.add(stretched);

					if (scene.activeObjects[i].collider.isInBounds(calcPoint))
					{
						//determine in between which vector angles ray is
						var angles = points.map(p => Ray.mapPointToPointAndAngle(p, origin));

						pointLoop:
						for (let pa = 0; pa < angles.length; pa++) 
						{
							for (let pb = 0; pb < angles.length; pb++)
							{
								if (angles[pa].angle - angles[pb].angle > 270 || angles[pb].angle - angles[pa].angle > 270)
								{

									castAngle = (castAngle + 180) % 360;
									angles.forEach(pIn => pIn.angle = (pIn.angle + 180) % 360);
									break pointLoop;
								}
							}
						}

						angles = angles.sort((a, b) => b.angle - a.angle);

						if (angles[1].length > angles[0].length && angles[1].length > angles[2].length)
						{
							//only one face can be hit at this time, which is between angles[0] and angles[2]
							angles.removeAt(1); //remove the center, connecting to the un-hittable face
						}
						else
						{
							//one of two faces can be hit at this time, either angles[0]-angles[1] or angles[1]-angles[2]
							(castAngle > angles[1].angle) ? angles.pop() : angles.shift(); //figure out on which side we are, and remove the furthest
						}

						//calc % of angle in relation to the angles it is between
						const smallAngle = Math.min(angles[1].angle, angles[0].angle);
						const percent = (castAngle - smallAngle) / (Math.max(angles[1].angle, angles[0].angle) - smallAngle);

						hits.push({ hitObj: scene.activeObjects[i], hitPoint: angles[1].point.lerpTo(angles[0].point, percent) });
						break;
					}
				}
			}
		}

		let shortest = hits[0];

		for (let i = 0; i < hits.length; i++)
		{
			if (hits[i].length < shortest.length)
			{
				shortest = hits[i];
			}
		}

		return shortest;
	}

	static mapPointToPointAndAngle (point, origin) 
	{
		const delta = point.subtract(origin);

		return {
			point: point,
			angle: delta.toAngle(),
			length: delta.length
		};
	}
}