// Basic event listeners, modeled after Phaser Signals

class Signal
{
	listeners = [];

	dispatch ()
	{
		for (var i = 0; i < this.listeners.length; i++)
		{
			this.listeners[i].callback.apply(this.listeners[i].context, arguments);
		}

		for (var i = this.listeners.length - 1; i > 0; i--)
		{
			if (this.listeners[i].once)
			{
				this.listeners.splice(i, 1);
			}
		}
	}

	add (callback, context, once)
	{
		if (!this.contains(callback, context))
		{
			this.listeners.push({ callback: callback, context: context, once: once });
		}
	}

	addOnce (callback, context)
	{
		this.add(callback, context, true);
	}

	contains (callback, context)
	{
		for (var i = this.listeners.length - 1; i > 0; i--)
		{
			if (this.listeners[i].callback === callback && this.listeners[i].context === context)
			{
				return true;
			}
		}

		return false;
	}

	remove (callback, context)
	{
		for (var i = this.listeners.length - 1; i > 0; i--)
		{
			if (this.listeners[i].callback === callback && this.listeners[i].context === context)
			{
				this.listeners.splice(i, 1);
				break;
			}
		}
	}

	removeAll ()
	{
		this.listeners = [];
	}
}
