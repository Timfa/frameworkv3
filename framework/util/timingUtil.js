/**
 /* Timing utility
 /*/

timingUtil =
{
	delayUntilCondition: function (conditionFunction, conditionContext, callback, context, interval)
	{
		interval = interval || 1;

		if (conditionFunction.call(conditionContext))
		{
			callback.call(context);
		}
		else
		{
			setTimeout(function ()
			{
				TimingUtil.delayUntilCondition(conditionFunction, conditionContext, callback, context, interval);
			}, interval);
		}
	},

	delayUntilNextFrame: function (callback, context)
	{
		Game.queuedCallbacks.push({ callback: callback, context: context });
	},

	delay: function (milliseconds, callback, context)
	{
		return setTimeout(function ()
		{
			callback.call(context);
		}, milliseconds);
	},

	stop: function (timer)
	{
		clearTimeout(timer);
	},

	repeat: function (milliseconds, callback, context)
	{
		return setInterval(function ()
		{
			callback.call(context);
		}, milliseconds);
	}
};